package main

import (
	"bufio"
	"os"

	"github.com/go-micro/plugins/v4/client/grpc"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"go-micro.dev/v4"

	"gitlab.com/rackward/messenger/app/chat"
	"gitlab.com/rackward/messenger/app/proto/app"
	"gitlab.com/rackward/messenger/srv/proto/srv"
)

func main() {
	log := logrus.New()

	displayName := "default"

	svc := micro.NewService(
		micro.Client(grpc.NewClient()),
		micro.Name(app.QualifiedName),
	)

	svc.Init(
		micro.Flags(&cli.StringFlag{
			Name:        "display_name",
			Aliases:     []string{"d"},
			Value:       "default",
			Required:    true,
			Usage:       "display name to use when chatting",
			Destination: &displayName,
		}),
	)

	chatClient := srv.NewMessengerService(srv.QualifiedName, svc.Client())

	user := &srv.User{
		Id:          uuid.NewString(),
		DisplayName: displayName,
	}

	app := chat.NewChatApp(log, chatClient, bufio.NewReader(os.Stdin), user)

	// initate app loop.
	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
