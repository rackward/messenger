package chat

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/rackward/messenger/srv/proto/srv"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type ChatApp struct {
	log     logrus.FieldLogger
	chatSvc srv.MessengerService
	// reader is where we will poll for messages, probably stdin.
	reader *bufio.Reader
	user   *srv.User
}

func NewChatApp(
	log logrus.FieldLogger,
	chatSvc srv.MessengerService,
	reader *bufio.Reader,
	user *srv.User,
) *ChatApp {
	return &ChatApp{
		log:     log,
		chatSvc: chatSvc,
		reader:  reader,
		user:    user,
	}
}

func (a *ChatApp) Run() error {
	// receive messages
	go func() {
		if err := a.receiveMessage(); err != nil {
			log.Fatal(err)
		}
	}()

	// poll for input and send messages
	a.pollForMessages()

	return nil
}

func (a *ChatApp) receiveMessage() error {
	// start streaming messages
	stream, err := a.chatSvc.StreamMessages(context.Background(), &srv.StreamMessagesRequest{
		User: a.user,
	})
	if err != nil {
		return err
	}

	defer stream.Close()

	a.log.WithField("user", a.user).Info("connection to chat server established")

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			a.log.Fatal(err)
		}

		// build message string
		str := fmt.Sprintf("%s: %s", msg.Sender.DisplayName, msg.Body)

		a.log.Info(str)
	}

	return nil
}

func (a *ChatApp) pollForMessages() error {
	for {
		text, err := a.reader.ReadString('\n')
		if err != nil {
			return err
		}

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		if _, err := a.chatSvc.CreateMessage(ctx, &srv.CreateMessageRequest{
			Message: &srv.Message{
				Body:      text,
				Timestamp: timestamppb.Now(),
				Sender:    a.user,
			},
		}); err != nil {
			a.log.Error(err)
		}
	}
}
