package handler

import (
	"context"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/rackward/messenger/srv/proto/srv"
)

type DefaultHandler struct {
	log       logrus.FieldLogger
	listeners map[string]chan *srv.Message
}

var _ srv.MessengerServiceHandler = (*DefaultHandler)(nil)

func NewDefaultHandler(
	log logrus.FieldLogger,
	messageChannel chan *srv.Message,
) *DefaultHandler {
	return &DefaultHandler{
		log:       log,
		listeners: make(map[string]chan *srv.Message),
	}
}

func (h *DefaultHandler) StreamMessages(ctx context.Context, req *srv.StreamMessagesRequest, rsp srv.MessengerService_StreamMessagesStream) error {
	ch := make(chan *srv.Message)
	// cleanup channel and remove listener
	defer func() {
		delete(h.listeners, req.User.Id)
		close(ch)
	}()

	h.listeners[req.User.Id] = ch

	for {
		msg := <-ch

		if err := rsp.Send(msg); err != nil {
			return err
		}
	}
}

func (h *DefaultHandler) CreateMessage(ctx context.Context, req *srv.CreateMessageRequest, rsp *srv.Message) error {
	msg := req.Message
	msg.Id = uuid.NewString()

	for k, l := range h.listeners {
		// don't send messages to the user who created the message.
		if k == req.Message.Sender.Id {
			continue
		}

		// attempt to send the message to the listener channel.
		select {
		case l <- msg:
			h.log.WithFields(logrus.Fields{
				"msg":  msg.Body,
				"user": k,
			}).Info("published message to channel")
		default:
			h.log.Warn("channel is full")
		}
	}

	*rsp = *msg

	return nil
}
