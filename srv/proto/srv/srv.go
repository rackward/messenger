package srv

//go:generate protoc --proto_path=$GOSRC/gitlab.com/rackward/messenger/protobuf --micro_out=$GOSRC --go_out=$GOSRC srv.proto

const (
	QualifiedName = "rapp.messenger.srv"
)
