package main

import (
	cgrpc "github.com/go-micro/plugins/v4/client/grpc"
	"github.com/go-micro/plugins/v4/server/grpc"
	"github.com/sirupsen/logrus"
	"go-micro.dev/v4"

	"gitlab.com/rackward/messenger/srv/handler"
	"gitlab.com/rackward/messenger/srv/proto/srv"
)

func main() {
	log := logrus.New()

	svc := micro.NewService(
		micro.Server(grpc.NewServer()),
		micro.Client(cgrpc.NewClient()),
		micro.Name(srv.QualifiedName),
		micro.Address(":8080"),
	)

	msgCh := make(chan *srv.Message, 10)

	handler := handler.NewDefaultHandler(log, msgCh)

	srv.RegisterMessengerServiceHandler(svc.Server(), handler)

	svc.Init()

	if err := svc.Run(); err != nil {
		log.Fatal(err)
	}
}
